import React from 'react';

import classes from './Banner.module.css';

interface IProps {
  title: string;
  tagline: string;
}

function Banner({ title, tagline}: IProps): JSX.Element {
  return (
    <div className={classes.Banner}>
      <hgroup className={classes.BannerText}>
        <h1 className={classes.BannerHeaderText}>{ title }</h1>
        <h3>{ tagline }</h3>
      </hgroup>
    </div>
  );
}

export { Banner };
