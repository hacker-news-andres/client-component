import React, { MouseEventHandler } from 'react';

import classes from './ArticlesItem.module.css';

interface IProps {
  title: string;
  author: string;
  pubdate: string;
  url: string;
  clicked: MouseEventHandler<HTMLSpanElement>
}

function ArticleItem({ title, author, pubdate, url, clicked }: IProps): JSX.Element {
  return (
    <article className={classes.ArticleItem}>
      <a href={url} target="_blank" rel="noreferrer">
        <div className={classes.ArticleTitle}>
          <p>{ title }</p>
          <p className={classes.ArticleAuthor}>- { author } -</p>
        </div>
      </a>
      <div className={classes.ArticlePubdate}>
        <p>{ pubdate }</p>
      </div>
      <div className={classes.ArticleActions}>
        <span className={[classes.Icon, classes.TrashIcon].join(' ')} onClick={clicked}></span>
      </div>
    </article>
  );
}

export { ArticleItem };
