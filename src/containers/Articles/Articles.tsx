import React from 'react';

import classes from './Articles.module.css';

interface IProps {
  children: React.ReactNode
}

function Articles({ children }: IProps): JSX.Element {
  return (
    <section className={classes.Articles}>
      { children }
    </section>
  );
}

export { Articles };
