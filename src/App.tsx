import React, { useEffect, useState } from 'react';

import { Banner } from './componets/UI/Banner/Banner';
import { Articles } from './containers/Articles/Articles';
import { ArticleItem } from './componets/ArticleItem/ArticleItem';

import { API_URL } from './utils/constants';

import dayjs from 'dayjs';
import relativeTime from 'dayjs/plugin/relativeTime';

function App(): JSX.Element {
  const [articles, setArticles] = useState([]);
  const [loadingArticles, setLoadingArticles] = useState(true);

  useEffect(() => {
    fetchArticles();
  }, []);

  const fetchArticles = async () => {
    try {
      let data: any = await fetch(`${API_URL}/articles`);
      data = await data.json();
      setLoadingArticles(false);
      setArticles(data.data);
    } catch (error) {
      setLoadingArticles(false);
      console.log(error);
      alert('It is not possible to fetch.');
    }
  };

  const removedHandler = async (id: string) => {
    try {
      let request: any = await fetch(`${API_URL}/articles/${id}`, { method: 'DELETE' });
      request = await request.json();
      const updatedArticles = articles.filter((value: any) => value._id !== id);
      setArticles(updatedArticles);
      alert(request.message);
    } catch (error) {
      console.log(error);
      alert('Cannot delete the article specified.')
    }
  }

  return (
    <React.Fragment>
      <Banner title='HN Feed' tagline='We ❤️ hacker news!' />
      <Articles>
        {loadingArticles && <p>Loading articles...</p>}
        {
          articles && articles.map((value: any) => {
            const pubdateToday: number = parseInt(dayjs(value.pubdate).format('D'));
            const today: number = new Date().getDate();
            let date: string = '';

            if (pubdateToday === today) {
              date = dayjs(value.pubdate).format('hh:mm a');
            } else {
              dayjs.extend(relativeTime);
              date = dayjs(value.pubdate).fromNow();
            }
            
            return (
              <ArticleItem 
                key={value._id}
                title={value.title}
                author={value.author}
                pubdate={ date } 
                url={value.url}
                clicked={() => removedHandler(value._id)}
              />
            );
          })
        }
        { articles.length === 0 ? <p>No article posted yet. </p> : null }
      </Articles>
    </React.Fragment>
  );
}

export default App;
